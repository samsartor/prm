use std::path::{Path, PathBuf};
use std::pin::Pin;

use async_fs as fs;
use clap::Parser;
use futures_lite::future::block_on;
use futures_util::stream::{iter, StreamExt, TryStreamExt};
use std::env::set_var;
use std::future::Future;
use std::io::Error;

/// Recursive remove in parallel
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Paths to recursively remove
    paths: Vec<PathBuf>,
    #[clap(flatten)]
    params: Params,
}

#[derive(Parser, Debug, Clone, Copy)]
struct Params {
    /// Maximum number of threads to spawn
    #[clap(short, long, default_value_t = 500)]
    threads: usize,

    /// Print out removed paths
    #[clap(short, long)]
    verbose: bool,
}

async fn remove_file(path: &Path, params: Params) -> Result<(), Error> {
    fs::remove_file(path).await?;
    if params.verbose {
        println!("{}", path.display());
    }
    Ok(())
}

async fn remove_dir(path: &Path, params: Params) -> Result<(), Error> {
    fs::remove_dir(path).await?;
    if params.verbose {
        println!("{}", path.display());
    }
    Ok(())
}

fn rm_path(
    path: PathBuf,
    ty: Option<fs::FileType>,
    params: Params,
) -> Pin<Box<dyn Future<Output = ()>>> {
    Box::pin(async move {
        match try_rm_path(&path, ty, params).await {
            Ok(ok) => ok,
            Err(err) => eprintln!("Could not remove {:?}: {}", path.display(), err),
        }
    })
}

async fn try_rm_path(path: &Path, ty: Option<fs::FileType>, params: Params) -> Result<(), Error> {
    match ty {
        None if remove_file(path, params).await.is_ok() => return Ok(()),
        Some(ty) if ty.is_file() => remove_file(path, params).await,
        _ => {
            fs::read_dir(path)
                .await?
                .map_ok(|entry| async move {
                    rm_path(entry.path(), entry.file_type().await.ok(), params).await;
                    Ok(())
                })
                .try_buffer_unordered(params.threads)
                .try_collect()
                .await?;
            remove_dir(path, params).await
        }
    }
}

fn main() {
    let Args { paths, params } = Args::parse();
    set_var("BLOCKING_MAX_THREADS", params.threads.to_string());
    block_on(
        iter(paths)
            .map(|path| rm_path(path, None, params))
            .buffer_unordered(params.threads)
            .collect::<()>(),
    );
}
